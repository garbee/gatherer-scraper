const puppeteer = require('puppeteer');
let args = process.argv.slice(2);

module.exports = async function() {
    return await puppeteer.launch({
        headless: args.includes('--no-headless') !== true,
    });
};
