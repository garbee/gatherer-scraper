const setupSettings = async function() {
    document.cookie = 'CardDatabaseSettings=0=1&1=28' +
    '&2=0&14=1&3=13&4=0&5=1&6=15&7=0&8=1&9=1&10=16&11=7&12=8&15=1&16=0&13=';
};

module.exports = async function(page) {
    await page.goto('http://gatherer.wizards.com')
    await page.evaluate(setupSettings);
    return page;
};
