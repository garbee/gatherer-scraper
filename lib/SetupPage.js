module.exports = async function(page) {
    const agent = await page.evaluate(() => window.navigator.userAgent);
    await page.setUserAgent(agent.replace('Headless', ''));
    await page.setViewport({
        width: 1024,
        height: 768,
    });

    return page;
};
