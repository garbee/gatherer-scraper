const fs = require('fs');

const browser = require('./Browser');
const setupPage = require('./SetupPage');

const getSetNames = function() {
    const selectNode = document.querySelector(
        '#ctl00_ctl00_MainContent_Content_SearchControls_setAddText'
    );
    return Array.from(
        selectNode.querySelectorAll('option')
    ).map((node) => node.value).filter((value) => value !== '').sort();
};

const getNames = async function(page) {
    await page.goto('http://gatherer.wizards.com/Pages/Default.aspx');
    const output = await page.evaluate(getSetNames);
    fs.writeFileSync('data/sets.json', JSON.stringify(output, null, 2));
    await page.close();
    return output;
};

module.exports = async function() {
    const browser1 = await browser();
    let page = await browser1.newPage();
    await setupPage(page);
    const names = await getNames(page);
    await browser1.close();
    return names;
};
