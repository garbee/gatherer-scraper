const browser = require('./Browser');
const setupPage = require('./SetupPage');
const fs = require('fs');

const getData = function(name) {
    return function() {
        const selectors = new Map();
        selectors.set('name', 'nameRow');
        selectors.set('manaCost', 'manaRow');
        selectors.set('convertedManaCost', 'cmcRow');
        selectors.set('rulesText', 'textRow');
        selectors.set('flavorText', 'flavorRow');
        selectors.set('powerAndToughness', 'ptRow');
        selectors.set('rarity', 'rarityRow');
        selectors.set('artist', 'artistRow');
        selectors.set('number', 'numberRow');

        const output = {};
        let nameNodes = Array.from(document.querySelectorAll(`[id$="${selectors.get('name')}"] .value`));
        let index = 0;
        nameNodes.forEach((node, count) => {
            if (node.textContent.trim() === name.trim()) {
                index = count;
            }
        });

        const dataContainer = document.querySelectorAll('.rightCol')[index];

        let types = dataContainer.querySelector('[id$="typeRow"] .value').textContent.trim().split('—').map((text) => text.trim());
        output['type'] = types;

        selectors.forEach((value, key) => {
            const get = dataContainer.querySelector(`[id$="${value}"] .value`);

            if (get === null) {
                return;
            }

            if (key === 'manaCost') {
                const textCost = Array.from(
                    get.querySelectorAll('img')
                ).map((node) => node.alt.trim());
                output[key] = textCost;
                return;
            }

            if (key === 'powerAndToughness' &&
                ! types.includes('Planeswalker')) {
                const PowTou = get.textContent.trim().split('/');
                output['power'] = PowTou[0].trim();
                output['toughness'] = PowTou[1].trim();
                return;
            }

            if (key === 'powerAndToughness' &&
                types.includes('Planeswalker')) {
                    output['loyalty'] = get.textContent.trim();
                    return;
                }

            if (key === 'rulesText') {
                output[key] = [];
                get.querySelectorAll('.cardtextbox').forEach((textBox) => {
                    textBox.querySelectorAll('img').forEach((image) => {
                      const text = image.alt;
                      image.outerHTML = `{${text}}`;
                    });
                    const pushText = textBox.textContent.trim();
                    if (pushText !== '') {
                      output[key].push(textBox.textContent.trim());
                    }
                 });

                return;
            }

            output[key] = get.textContent.trim();
        });

        return output;
    };
};

module.exports = async function(setName) {
    const browser1 = await browser();
    const fileName = `data/sets/${setName}.json`;
    let outputData = [];
    let data = JSON.parse(fs.readFileSync(fileName));

    return data.reduce((current, item) => {
        return current.then(async () => {
            let page = await browser1.newPage();
            await setupPage(page);
            await page.goto(item.url);
            let output = await page.evaluate(getData(item.name));
            outputData.push(Object.assign({}, item, output));
            return page.close();
        });
    }, Promise.resolve()).then(() => {
        fs.writeFileSync(fileName, JSON.stringify(outputData, null, 2));
        return browser1.close();
    });
};
