const getSetByName = require('./GetSetByName');
const getSetNames = require('./GetSetNames');

getSetNames().then((names) => {
  names.reduce((promise, name) => {
    return getSetByName(name).then(() => {
      console.log(`Done with ${name}`);
    });
  }, Promise.resolve());
});
