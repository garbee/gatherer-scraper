const fs = require('fs');

const browser = require('./Browser');
const setupPage = require('./SetupPage');
const setupSettings = require('./SetupSettings');

const getCardList = function() {
    return Array.from(
        document.querySelectorAll('.name a')
    ).map((node) => {
        const reference = new URL(node.href);
        return {
            name: node.textContent.trim(),
            url: reference.href,
            multiverseid: reference.searchParams.get('multiverseid'),
        };
    });
};

module.exports = async function(name) {
    const pageUrl = `http://gatherer.wizards.com/Pages/Search/Default.aspx?set=["${encodeURI(name)}"]`;
    const browser1 = await browser();
    let page = await browser1.newPage();
    await setupPage(page);
    await setupSettings(page);
    await page.goto(pageUrl);
    let results = await page.evaluate(getCardList);
    fs.writeFileSync(`data/sets/${name}.json`, JSON.stringify(results, null, 2));
    return browser1.close();
};
