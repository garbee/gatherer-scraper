const getSetNames = require('./lib/GetSetNames');
const getAllSets = require('./lib/GetAllSets');
const getSetByName = require('./lib/GetSetByName');

module.exports = {
  getSetNames,
  getAllSets,
  getSetByName,
};
