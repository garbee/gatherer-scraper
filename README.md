# Gatherer Scraper

Gatherer Scraper is a library to pull card data from Wizard's Gatherer database into uniform JSON files.
The data obtained is a pure scrape with as little data manipulation as possible.
Scraping is done through a Chrome Headless instance using the [puppeteer](https://github.com/GoogleChrome/puppeteer) library from the Chrome team.

## Usage

1. Clone the repository
2. `yarn install` This may take a while, as puppeteer downloads its own internal Chromium instance to use.
3. There are a few commands available under the CLI path to handle different tasks.
    * `node cli/GetSetnames.js` will obtain all set names into `data/sets.json`. This is the full list of acceptable names to provide for scraping.
    * `node cli/GetSets.js` takes a list of set names which you would like to get the data to. The names are a arguments separated by a space. The data for each set is stored in `data/sets/{setName}.js`.
    * `node cli/GetAllCardData.js` will retrieve all card information available for every set. This will take a very long time.

When running locally for debugging, you may provide `--no-headless` to any CLI command to run in non-headless mode. This will allow you to see exactly what is happening for any command.

For example, to only get the Return to Ravnica and Innistrad card data, you would run `node cli/GetSets.js "Return to Ravnica" Innistrad`.
