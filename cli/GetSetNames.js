const getSetNames = require('../lib/GetSetNames');

getSetNames().then(() => {
    console.log('Set names saved to data/sets.json');
}).then(() => {
    process.exit(0);
});
