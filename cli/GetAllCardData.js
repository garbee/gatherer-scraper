const getSetNames = require('../lib/GetSetNames');
const getSetByName = require('../lib/GetSetByName');
const getCards = require('../lib/GetCards');

getSetNames().then((names) => names.reduce(
    (current, name) => {
        return current.then(
            () => getSetByName(name)
        ).then(
            () => getCards(name)
        ).then(
            () => {
                console.log(name + ' set data retrieved');
            });
    },
    Promise.resolve()
).then(() => {
    console.log('All done');
    process.exit(0);
}));
