let args = process.argv.slice(2);
const programArgs = [
    '--no-headless',
];
const getSetByName = require('../lib/GetSetByName');
const getCards = require('../lib/GetCards');

let names = args.filter((arg) => {
    return programArgs.includes(arg) === false;
});

names.reduce(
    (current, name) => {
        return current.then(
            () => getSetByName(name)
        ).then(
            () => getCards(name)
        ).then(
            () => {
                console.log(name + ' set data retrieved');
            });
    },
    Promise.resolve()
).then(() => {
    console.log('All done');
    process.exit(0);
});
